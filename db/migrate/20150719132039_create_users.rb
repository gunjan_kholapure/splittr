class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|

      t.string :name
      t.string :email
      t.boolean :active
      t.string :password_digest
      t.integer :give
      t.integer :take
      t.timestamps null: false
      
    end
  end
end
